Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'home#index'

  namespace :api do
    namespace :v1 do
      get '/triangle-star-pattern/', to: 'answer#triangle_star_pattern'
      get '/fibonacci', to: 'answer#fibonacci'
      get '/reverse-array', to: 'answer#reverse_array'
      get '/remove-duplicate-array', to: 'answer#remove_duplicate_array'
      get '/print-duplicate-element', to: 'answer#print_duplicate_element'
      get '/largest-smallest-number', to: 'answer#largest_smallest_number'
    end
  end
end
