# README
This is the answer for Crowdo Technical Test.

## Architecture
* This application uses ReactJS on the frontend side.
And Ruby on Rails on the backend side.
* React code is compiled with Webpack (gem webpacker).

### Configuration
* Clone this repository and run `bundle install`
* Ruby version: 3.0.2
* Node version: 15.11.0
* Use yarn package manager
* System dependencies: rails ^6.1.4, bundle 2.2.25
