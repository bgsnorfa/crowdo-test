class Api::V1::AnswerController < ApplicationController

  def triangle_star_pattern
    choice = params[:choice].nil? ? 1 : params[:choice].to_i
    num = params[:num].nil? ? 5 : params[:num].to_i

    stars = ""

    case choice
    when 1
      num.times do |a|
        (a + 1).times { stars += "*" }          
        stars += "<br />" unless (a + 1) == num
      end
    when 2
      num.downto(1) do |a|
        a.times { stars += "*" }
        stars += "<br />" unless a == 1
      end
    when 3
      num.times do |a|
        spaces = num - a - 1
        spaces.times { stars += "&nbsp;&nbsp;" }
        (a + 1).times { stars += "*" }
        stars += "<br />" unless (a + 1) == num
      end
    when 4
      iter = 0
      num.times do 
        iter = iter + 1
        space = num - iter - 1
        space.times do
          stars += "&nbsp;"
        end
        iter.times do
          stars += "*"
        end
        stars += "<br/>"
      end
    end

    render json: {
      stars: stars
    }
  end

  def fibonacci
    n = params[:n].nil? ? 5 : params[:n].to_i

    fib = FibonacciService.new(n)

    render json: {
      array: fib.call.to_s
    }
  end

  def reverse_array
    array = params[:array].nil? ? [] : str_to_arr(params[:array])

    arr = ArrayService.new(array)

    render json: {
      array: arr.call('reverse').to_s
    }
  end

  def remove_duplicate_array
    array = params[:array].nil? ? [] : str_to_arr(params[:array])

    arr = ArrayService.new(array)

    render json: {
      array: arr.call('remove_duplicate').to_s
    }
  end

  def print_duplicate_element
    array = params[:array].nil? ? [] : str_to_arr(params[:array])
    
    arr = ArrayService.new(array)

    render json: {
      duplicates: arr.call('print_duplicate_element')
    }
  end

  def largest_smallest_number
    array = params[:array].nil? ? [] : str_to_arr(params[:array])
    
    arr = ArrayService.new(array)

    render json: arr.call('largest_smallest_number')
  end

  private

  def str_to_arr(str)
    unless str.nil?
      str.tr('[]', '').split(',').map(&:to_i)
    else
      str = []
    end
  end

end
