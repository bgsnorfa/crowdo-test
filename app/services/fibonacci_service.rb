class FibonacciService
  
  def initialize(n)
    @n = n  
  end

  def call
    arr = []
    @n.times do |idx|
      arr.push(calculate_fibonacci idx)
    end
    return arr
  end

  private 
  
  def calculate_fibonacci(n)
    return n if n <= 1
    calculate_fibonacci(n - 1) + calculate_fibonacci(n - 2)
  end
end