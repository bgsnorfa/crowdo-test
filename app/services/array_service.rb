class ArrayService

  def initialize(arr)
    @arr = arr
  end

  def call(type)
    case type
    when 'reverse'
      array = []
      (@arr.length - 1).downto(0).each do |idx|
        array.push(@arr[idx])
      end

      return array

    when 'remove_duplicate'
      return @arr.uniq

    when 'print_duplicate_element'
      duplicated_elem = Hash.new(0)

      @arr.each do |val|
        duplicated_elem[val] += 1
      end

      duplicated_elem.each do |key, val|
        if val == 1
          duplicated_elem.delete(key)
        end
      end

      return duplicated_elem

    when 'largest_smallest_number'
      numbers = {
        largest: 0,
        smalelst: 0
      }

      if @arr.any?
        @arr = @arr.uniq
        @arr = @arr.sort_by(&:to_i)
        numbers = {
          largest: @arr[@arr.length - 1],
          smallest: @arr[0]
        }
      end

      return numbers

    else
      return []
    end
  end
end