import React, { useState } from 'react';
import ReactHtmlParser from 'react-html-parser';

const ReverseArray = () => {
  const [array, setArray] = useState("[1, 2, 3, 4, 5]");
  const [result, setResult] = useState("");

  const handleArrayChange = (e) => {
    setArray(e.target.value);
  }

  const handleShow = (e) => {
    e.preventDefault();
    fetchResult(array)
      .then(data => {
        setResult(data.array);
      })
  }

  const fetchResult = async (array) => {
    const response = await fetch(`/api/v1/reverse-array?array=${array}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });

    return response.json();
  }

  return (
    <div>
      <form className="form-inline">
        <label>Array:</label>
        <input
          type="text"
          className="form-control"
          placeholder="Enter array"
          name="array"
          value={array}
          onChange={handleArrayChange}
        />

        <button
          type="submit"
          className="btn btn-primary"
          onClick={handleShow}
        >Show</button>
      </form>

      <div className="card">
        {ReactHtmlParser(result)}
      </div>
    </div>
  );
}

export default ReverseArray;