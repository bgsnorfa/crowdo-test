import React, { useState } from 'react';

const LargestSmallestNumber = () => {
  const [array, setArray] = useState("[1, 2, 3, 4, 5]");
  const [result, setResult] = useState({});

  const handleArrayChange = (e) => {
    setArray(e.target.value);
  }

  const handleShow = (e) => {
    e.preventDefault();
    fetchResult(array)
      .then(data => {
        setResult(data);
      })
  }

  const fetchResult = async (array) => {
    const response = await fetch(`/api/v1/largest-smallest-number?array=${array}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });

    return response.json();
  }

  return (
    <div>
      <form className="form-inline">
        <label>Array:</label>
        <input
          type="text"
          className="form-control"
          placeholder="Enter array"
          name="array"
          value={array}
          onChange={handleArrayChange}
        />

        <button
          type="submit"
          className="btn btn-primary"
          onClick={handleShow}
        >Show</button>
      </form>

      <div className="card">
        {Object.keys(result).length !== 0 &&
          <p>
            smallest number = {result['smallest']}
            <br />
            largest number = {result['largest']}
          </p>
        }
      </div>
    </div>
  );
}

export default LargestSmallestNumber;