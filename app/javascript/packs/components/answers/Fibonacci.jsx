import React, { useState } from 'react';
import ReactHtmlParser from 'react-html-parser';

const Fibonacci = () => {
  const [n, setN] = useState(5);
  const [fibonacci, setFibonacci] = useState("");

  const handleNChange = (e) => {
    setN(e.target.value);
  }

  const handleShow = (e) => {
    e.preventDefault();
    fetchFibonacci(n)
      .then(data => {
        setFibonacci(data.array);
      })
  }

  const fetchFibonacci = async (n) => {
    const response = await fetch(`/api/v1/fibonacci?n=${n}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });

    return response.json();
  }

  return (
    <div>
      <form className="form-inline">
        <label>N:</label>
        <input
          type="number"
          className="form-control"
          placeholder="Enter n"
          name="n"
          value={n}
          onChange={handleNChange}
        />

        <button
          type="submit"
          className="btn btn-primary"
          onClick={handleShow}
        >Show</button>
      </form>

      <div className="card">
        {ReactHtmlParser(fibonacci)}
      </div>
    </div>
  );
}

export default Fibonacci;