import React from 'react';

const CodeIssue  = () => {

  return (
    <div className="card">
      <p>
        The code below will throw an error ("created_at" is declared but it can cause ambiguity)
        <br />
        because of the query is using joins (from 2 models) and we need to explicitly declare the table we are using 
        <br />
        when calling column "created_at"
      </p>
      <pre>
        {`articles = Article.joins(:comments)
          .where("created_at >= ? AND created_at < ?", date1, date2) // this line will cause trouble
          .where("publish_date >= #{params[:date]}")
          .where(genre: 'Pop')
          .where("comments.created_at >= #{params[:date]}")
          .select('articles.title')`}
      </pre>
      <p>
        Code fix: declare the table name when using any column
      </p>
      <pre>
        {`articles = Article.joins(:comments)
          .where("articles.created_at >= ? AND articles.created_at < ?", date1, date2)
          .where("articles.publish_date >= #{params[:date]}")
          .where(articles.genre: 'Pop')
          .where("comments.created_at >= #{params[:date]}")
          .select('articles.title')`}
      </pre>
    </div>
  );
}

export default CodeIssue;