import React, { useState } from 'react';

const PrintDuplicateElement = () => {
  const [array, setArray] = useState("[1, 2, 3, 4, 4, 5, 5]");
  const [result, setResult] = useState("");

  const a = {
    a: 1, 
    b: 2,
    c: 3
  }

  const handleArrayChange = (e) => {
    setArray(e.target.value);
  }

  const handleShow = (e) => {
    e.preventDefault();
    fetchResult(array)
      .then(data => {
        setResult(data.duplicates);
      })
  }

  const fetchResult = async (array) => {
    const response = await fetch(`/api/v1/print-duplicate-element?array=${array}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });

    return response.json();
  }

  return (
    <div>
      <form className="form-inline">
        <label>Array:</label>
        <input
          type="text"
          className="form-control"
          placeholder="Enter array"
          name="array"
          value={array}
          onChange={handleArrayChange}
        />

        <button
          type="submit"
          className="btn btn-primary"
          onClick={handleShow}
        >Show</button>
      </form>

      <div className="card">
        {result !== "" &&
          Object.keys(result).map((key) => {
            return <div key={key}>{key} = {result[key]}</div>
          })
        }
      </div>
    </div>
  );
}

export default PrintDuplicateElement;