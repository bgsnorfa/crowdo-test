import React from 'react';

const CodeRefactor  = () => {

  return (
    <div className="card">
      <p>
        Existing code:
      </p>
      <pre>
        {`
          class ArticlesController < ApplicationController
            def update
              article = Article.find(params[:id])

              article.update(form_params)

              if article.genre == 'Pop'
                article.publish_date = Date.today + 1.month
                article.status = 'pending_review'
              elsif article.genre == 'Animals'
                article.publish_date = Date.today + 2.weeks
                article.status = 'pending_approval'
              end
              article.save!

              if article.status == 'pending_review'
                writer = article.writer
                if writer.articles.count > 10
                  writer.status = 'pending_upgrade_review'
                  writer.save!
                  SendWriterUpgradeMail.perform_now(writer)
                end
              end
            end
          end
        `}
      </pre>
      <p>
        Refactor: 
      </p>
      <ul>
        <li>Make the code neater and scalable (change if-elsif-end with case when)</li>
        <li>make the code perform efficiently (change article.update into article.attributes so it won't write to DB, and only commit after all changes are ready to be written to DB by performing article.save!)</li>
        <li>Use perform_later for heavy task (asynchronous)</li>
      </ul>
      <pre>
        {`
          class ArticlesController < ApplicationController
            def update
              article = Article.find(params[:id])

              # article.update(form_params)
              article.attributes(form_params)

              case article.genre
              when 'Pop'
                article.publish_date = Date.today + 1.month
                article.status = 'pending_review'
              when 'Animals'
                article.publish_date = Date.today + 2.weeks
                article.status = 'pending_approval'
              end

              article.save!

              if article.status == 'pending_review'
                writer = article.writer
                if writer.articles.count > 10
                  writer.status = 'pending_upgrade_review'
                  writer.save!
                  SendWriterUpgradeMail.perform_later(writer)
                end
              end

            end
          end
        `}
      </pre>
    </div>
  );
}

export default CodeRefactor;