import React, { useState } from 'react';
import ReactHtmlParser from 'react-html-parser';

const TriangleStar = () => {
  const [choice, setChoice] = useState(1);
  const [num, setNum] = useState(5);
  const [stars, setStars] = useState("");

  const handleNumChange = (e) => {
    setNum(e.target.value);
  }

  const handleChoiceChange = (e) => {
    setChoice(e.target.value);
  }

  const handleShow = (e) => {
    e.preventDefault();
    fetchStars(choice, num)
      .then(data => {
        setStars(data.stars);
      })
  }

  const fetchStars = async (choice, num) => {
    const response = await fetch(`/api/v1/triangle-star-pattern?choice=${choice}&num=${num}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    });

    return response.json();
  }

  return (
    <div>
      <form className="form-inline">
        <label>Choice:</label>
        <select 
          className="form-control" 
          value={choice} 
          onChange={handleChoiceChange}
        >
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
        </select>

        <label>Num:</label>
        <input 
          type="number"
          className="form-control"
          placeholder="Enter num"
          name="num"
          value={num}
          onChange={handleNumChange}
        />

        <button 
          type="submit" 
          className="btn btn-primary"
          onClick={handleShow}
        >Show</button>
      </form>
      
      <div className="card">
        {ReactHtmlParser(stars)}
      </div>
    </div>
  );
}

export default TriangleStar;