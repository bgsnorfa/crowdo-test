import React from 'react';
import Fibonacci from './answers/Fibonacci';
import ReverseArray from './answers/ReverseArray';
import TriangleStar from './answers/TriangleStar';
import RemoveDuplicateArray from './answers/RemoveDuplicateArray';
import PrintDuplicateElement from './answers/PrintDuplicateElement';
import LargestSmallestNumber from './answers/LargestSmallestNumber';
import CodeIssue from './answers/CodeIssue';
import CodeRefactor from './answers/CodeRefactor';

const Accordion = () => {
  return (
    <div id="accordion">
      <div className="card">
        
        <>
          <div className="card-header" id="triangleStarButton">
            <h5 className="mb-0">
              <button className="btn btn-link" data-toggle="collapse" data-target="#triangleStar" aria-expanded="true" aria-controls="triangleStar">
                [1] Triangle Star Pattern
              </button>
            </h5>
          </div>

          <div id="triangleStar" className="collapse" aria-labelledby="triangleStar" data-parent="#accordion">
            <div className="card-body">
              <TriangleStar />
            </div>
          </div>
        </>
        
        <>
          <div className="card-header" id="fibonacciButton">
            <h5 className="mb-0">
              <button className="btn btn-link" data-toggle="collapse" data-target="#fibonacci" aria-expanded="true" aria-controls="fibonacci">
                [2] Fibonacci
              </button>
            </h5>
          </div>

          <div id="fibonacci" className="collapse" aria-labelledby="fibonacci" data-parent="#accordion">
            <div className="card-body">
              <Fibonacci />
            </div>
          </div>
        </>
        
        <>
          <div className="card-header" id="reverseArrayButton">
            <h5 className="mb-0">
              <button className="btn btn-link" data-toggle="collapse" data-target="#reverseArray" aria-expanded="true" aria-controls="reverseArray">
                [3] Reverse Array
              </button>
            </h5>
          </div>

          <div id="reverseArray" className="collapse" aria-labelledby="reverseArray" data-parent="#accordion">
            <div className="card-body">
              <ReverseArray />
            </div>
          </div>
        </>
        
        <>
          <div className="card-header" id="removeDuplicateArrayButton">
            <h5 className="mb-0">
              <button className="btn btn-link" data-toggle="collapse" data-target="#removeDuplicateArray" aria-expanded="true" aria-controls="removeDuplicateArray">
                [4] Remove Duplicate Array
              </button>
            </h5>
          </div>

          <div id="removeDuplicateArray" className="collapse" aria-labelledby="removeDuplicateArray" data-parent="#accordion">
            <div className="card-body">
              <RemoveDuplicateArray />
            </div>
          </div>
        </>
       
        <>
          <div className="card-header" id="printDuplicateElementButton">
            <h5 className="mb-0">
              <button className="btn btn-link" data-toggle="collapse" data-target="#printDuplicateElement" aria-expanded="true" aria-controls="printDuplicateElement">
                [5] Print Duplicate Element
              </button>
            </h5>
          </div>

          <div id="printDuplicateElement" className="collapse" aria-labelledby="printDuplicateElement" data-parent="#accordion">
            <div className="card-body">
              <PrintDuplicateElement />
            </div>
          </div>
        </>
        
        <>
          <div className="card-header" id="largestSmallestNumberButton">
            <h5 className="mb-0">
              <button className="btn btn-link" data-toggle="collapse" data-target="#largestSmallestNumber" aria-expanded="true" aria-controls="largestSmallestNumber">
                [6] Largest Smallest Number
              </button>
            </h5>
          </div>

          <div id="largestSmallestNumber" className="collapse" aria-labelledby="largestSmallestNumber" data-parent="#accordion">
            <div className="card-body">
              <LargestSmallestNumber />
            </div>
          </div>
        </>
        
        <>
          <div className="card-header" id="codeIssueButton">
            <h5 className="mb-0">
              <button className="btn btn-link" data-toggle="collapse" data-target="#codeIssue" aria-expanded="true" aria-controls="codeIssue">
                [7] Code Issue
              </button>
            </h5>
          </div>

          <div id="codeIssue" className="collapse" aria-labelledby="codeIssue" data-parent="#accordion">
            <div className="card-body">
              <CodeIssue />
            </div>
          </div>
        </>
        
        <>
          <div className="card-header" id="codeRefactorButton">
            <h5 className="mb-0">
              <button className="btn btn-link" data-toggle="collapse" data-target="#codeRefactor" aria-expanded="true" aria-controls="codeRefactor">
                [8] Code Change / Refactor
              </button>
            </h5>
          </div>

          <div id="codeRefactor" className="collapse" aria-labelledby="codeRefactor" data-parent="#accordion">
            <div className="card-body">
              <CodeRefactor />
            </div>
          </div>
        </>



      </div>
    </div>
  );
}

export default Accordion;