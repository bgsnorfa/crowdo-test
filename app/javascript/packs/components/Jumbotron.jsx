import React from 'react';

const Jumbotron = () => {
  return (
    <div className="jumbotron text-center">
      <h1>Crowdo Technical Test</h1>
      <p>
        This page shows all of the required answers for technical test.
      </p>
    </div>
  );
}

export default Jumbotron;